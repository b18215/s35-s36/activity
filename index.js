/*------------------------SESSION 35~36 ----------------------------
-------------- ODM - Data Persistence via Mongoose ODM -------------


A. Terminal
	Commands via terminal

		1. npm init -y
		2. npm i express mongoose nodemon
		3. touch .gitignore index.js
		4. mkdir controllers models routes
		5. npm start


B. package.json
	In package.json add "start" : "nodemon index" in "scipts"


	{
	  "name": "s35-s36",
	  "version": "1.0.0",
	  "main": "index.js",
	  "scripts": {
	    "test": "echo \"Error: no test specified\" && exit 1",
	    "start" : "nodemon index" <-------------------------
	  },
	  "keywords": [],
	  "author": "",
	  "license": "ISC",
	  "description": "",
	  "dependencies": {
	    "express": "^4.18.1",
	    "mongoose": "^6.3.5",
	    "nodemon": "^2.0.16"
	  }
	}

C. .gitignore

	In .gitignore files, add "/node_modules"

*/

//---------------------END OF INITIAL SET-UP----------------------\\

/*

Folders & Files to create:

	1. index.js
	2. Folder/File: controllers/<name>Controllers.js
	3. Folder/File: models/<name>.js (SCHEMA)
	4. Folder/File: routes/<name>Routes.js


*/

const express = require('express');

// connection to MongoDB
const mongoose = require('mongoose');


//port
const port = 4000;

//server
const app = express();

//mongoose connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.s56h5.mongodb.net/tasks182?retryWrites=true&w=majority",

	// !important! additional code to avoid future error as MongoDB update versions
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}
);

// this will create a notification if the db connection is sucessful or not
let db = mongoose.connection

// for error connection notification
db.on('error', console.error.bind(console, "DB Connection Error!"));

//for sucsessfull connection notification
db.once('open', () => console.log("Successfully connected to MongoDB"));





//middlewares
app.use(express.json());

//reading of data forms
//usually string or array are being accepted, with this middleware, this will enable us to accet other data types.
app.use(express.urlencoded({extended: true}))


//Routes
const taskRoutes = require('./routes/taskRoutes');
app.use('/tasks', taskRoutes);





/*------------------------SESSION 35~36 ----------------------------
--------------------------- ACTIVITY 1-------------------------------


Activity 1

    >> Create a new schema for User. It should have the following fields:
        --username,
        --password
    The data types for both fields is String.

    >> Create a new model out of your schema and save it in a variable called User

    >> Create a new POST method route to create 3 new user documents:
        -endpoint: "/user"
        -This route should be able to create a new user document.
        -Then, send the result in the client.
        -Catch an error while saving, send the error in the client.
        -STRETCH GOAL: no duplicate usernames

    >> Create a new GET method route to retrieve all user documents:
        -endpoint: "/user"
        -Then, send the result in the client.
        -Catch an error, send the error in the client.

    >> Make sure to group your routes for user in index.js

    */


    //Routes
    const userRoutes = require('./routes/userRoutes');
    app.use('/users', userRoutes);






   /* --------------------------- ACTIVITY 2 ------------------------------

    Activity 2:

        >> Create 2 new routes and controllers in our userRoutes and userControllers.

        >> Create a route and controller to update a single user's username field to our input from a request body.
            -endpoint: '/:id'
            -Create a new controller which is able to get the id from the url through the use of req.params:
                -Create an updates object and add the new value from our req.body as the new value to the username field.

                -Add a findByIdAndUpdate method from the User model and pass the id from req.params as its first argument.

                -Add the updates object as its second argument.

                -Add {new:true} as its third argument so the result would return the updated document instead of the old one.

                -Then pass the result to the client.

                -Catch the error and pass the error to the client.

           
        >> Create a route and controller to get a single user.
                -endpoint: '/getSingleUser/:id'
               
                -Create a new controller which is able to get the id of the user from the url through the use of req.params.
                    -In the controller, add a findById method from the User model and pass the id from req.params as its argument.
                    -Then, send the result to the client.
                    -Catch the error and send the error to the client.*/
















//-------------------------Port Listener -------------------------------


    app.listen(port, () => console.log(`Server is running at port ${port}`))
//-----------------------------------------------------------------------