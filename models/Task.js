
// -------------------------SCHEMA--------------------------------

const mongoose = require('mongoose');

// Schema - a blueprint for our data/document 
const taskSchema = new mongoose.Schema({


	name: String,

	status: String

});

// ---------------------MONGOOSE MODEL----------------------------
/*
	mongoose.model(<nameOfCollectionInAtlas>, <schedmaToFollow>)

*/

module.exports = mongoose.model("Task", taskSchema);