
// -------------------------SCHEMA--------------------------------

const mongoose = require('mongoose');

// Schema - a blueprint for our data/document 
const userSchema = new mongoose.Schema({


	username: String,

	password: String

});

module.exports = mongoose.model("User", userSchema);