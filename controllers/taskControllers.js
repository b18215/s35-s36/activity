// import the Task model in our controllers. So that our controllers or controller function may have access to our Task model.
const Task = require("../models/Task");

//----------------------Create task----------------------------------

module.exports.createTaskControllers = (req, res) => {
	console.log(req.body);
//                   ---->.then make promise to make JS asynchronous
	Task.findOne({name: req.body.name}).then(result =>{
		console.log(result)

		if(result !== null && result.name === req.body.name){
			return res.send('Duplicate task found')
		} else {
			let newTask = new Task ({
				name: req.body.name,
				status: req.body.status
			})

			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

//----------------------Retrieve all tasks---------------------------

module.exports.getAllTasksController = (req, res) => {
	
	//similar to: db.tasks.find({})
	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


//----------------Retrieve of single task--------------------

module.exports.getSingleTaskController = (req, res) => {
	
	console.log(req.params);

	//similar to MongoDB command: db.tasks.findOne({_id: "id"})
	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


//--------------------Updating task status----------------
module.exports.updateTaskStatusController = (req, res) => {
	
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		status : req.body.status
	}


	//findByIDAndUpdate()
		// has 3 arguments
			// a. where will you get the id? = req.params.id
			// b. what is the update? = 
			// c. {new: true} - return the updated version of the document we are updating (option) default is not updated
	Task.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedTask => res.send(updatedTask))
	.catch(error => res.send(error));
};