// import the user model in our controllers. So that our controllers or controller function may have access to our user model.
const User = require("../models/User");

//----------------------Create user---------------------------

module.exports.createUserControllers = (req, res) => {
	console.log(req.body);
//                   ---->.then make promise to make JS asynchronous
	User.findOne({username: req.body.username}).then(result =>{
		console.log(result)

		if(result !== null && result.username === req.body.username){
			return res.send('Duplicate user found')
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
};

//----------------------Retrieve all user---------------------------

module.exports.getAllUsersController = (req, res) => {
	
	//similar to: db.user.find({})
	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

//--------------------Updating user username----------------
module.exports.updateUserUsernameController = (req, res) => {
	
	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		username : req.body.username
	}


	//findByIDAndUpdate()
		// has 3 arguments
			// a. where will you get the id? = req.params.id
			// b. what is the update? = 
			// c. {new: true} - return the updated version of the document we are updating (option) default is not updated
	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};

//----------------Retrieve of single User--------------------

module.exports.getSingleUserController = (req, res) => {
	
	console.log(req.params);

	//similar to MongoDB command: db.tasks.findOne({_id: "id"})
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};
