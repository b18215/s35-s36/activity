
const express = require('express');
const router = express.Router();
// Router method - allows access to HTTP methods

const userControllers = require('../controllers/userControllers');

console.log(userControllers);

//create user route
router.post('/', userControllers.createUserControllers);

//get all users route
router.get('/', userControllers.getAllUsersController);

//update single user route
router.put('/updateUser/:id', userControllers.updateUserUsernameController);

//get single user route
router.get('/getSingleUser/:id', userControllers.getSingleUserController);

module.exports = router;