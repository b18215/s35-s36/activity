
const express = require('express');
const router = express.Router();
// Router method - allows access to HTTP methods

const taskControllers = require('../controllers/taskControllers');

console.log(taskControllers);

//create task route
router.post('/', taskControllers.createTaskControllers);

//get all task route
router.get('/', taskControllers.getAllTasksController);

//get single task route
router.get('/getSingleTask/:id', taskControllers.getSingleTaskController);

//update single task route
router.put('/updateTask/:id', taskControllers.updateTaskStatusController);

module.exports = router;